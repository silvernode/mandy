***CHANGELOG***

Version 0.6.0 (8-5-13)

* Set the default settings in 'mandy.cfg'
* Can now select audio bitrate
* Autoremove old video files from /tmp
* Added option to play completed file
- Still unknown bug with 'converting' bar


Version 0.5.0 (7-31-13)

*added audio format selection for mp3. ogg, and flac
-autoplay has been removed, users now play files manually

Version 0.4 (7-27-13)

+ fixed media player error when opening finished file
+ fixed .mp3 validation
+ cancel button now works properly in url entry box
* added progress bar for conversion process
* added push notifications
* files now open with default media player not vlc


